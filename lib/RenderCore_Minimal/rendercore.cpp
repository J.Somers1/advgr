﻿
#include <iostream>
#include <fstream>
/* rendercore.cpp - Copyright 2019 Utrecht University

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

	   http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

#include "core_settings.h"

using namespace lh2core;

//  +-----------------------------------------------------------------------------+
//  |  RenderCore::Init                                                           |
//  |  Initialization.                                                      LH2'19|
//  +-----------------------------------------------------------------------------+
void RenderCore::Init()
{
	// initialize core

	//TODO Create BVH here
}

//  +-----------------------------------------------------------------------------+
//  |  RenderCore::SetTarget                                                      |
//  |  Set the OpenGL texture that serves as the render target.             LH2'19|
//  +-----------------------------------------------------------------------------+
void RenderCore::SetTarget( GLTexture* target )
{
	// synchronize OpenGL viewport
	targetTextureID = target->ID;
	if (screen != 0 && target->width == screen->width && target->height == screen->height) return; // nothing changed
	delete screen;
	screen = new Bitmap( target->width, target->height );
}

//  +-----------------------------------------------------------------------------+
//  |  RenderCore::SetGeometry                                                    |
//  |  Set the geometry data for a model.                                   LH2'19|
//  +-----------------------------------------------------------------------------+
void RenderCore::SetGeometry( const int meshIdx, const float4* vertexData, const int vertexCount, const int triangleCount, const CoreTri* triangleData, const uint* alphaFlags )
{
	Mesh newMesh;
	// copy the supplied vertices; we cannot assume that the render system does not modify
	// the original data after we leave this function.
	newMesh.vertices = new float4[vertexCount];
	newMesh.vcount = vertexCount;
	
	memcpy( newMesh.vertices, vertexData, vertexCount * sizeof( float4 ) );
	// copy the supplied 'fat triangles'
	newMesh.triangles = new CoreTri[vertexCount / 3];
	memcpy( newMesh.triangles, triangleData, (vertexCount / 3) * sizeof( CoreTri ) );
	meshes.push_back( newMesh );
}

//  +-----------------------------------------------------------------------------+
//  |  RenderCore::Render                                                         |
//  |  Produce one image.                                                   LH2'19|
//  +-----------------------------------------------------------------------------+
void RenderCore::Render( const ViewPyramid& view, const Convergence converge, const float brightness, const float contrast )
{

	// render
	screen->Clear();
	//to speed it up by factor 100, this simple code is added to create a boundingbox for each mesh:
	int nMeshes = meshes.size();
	std::tuple<float3, float3> boundingBoxes [2000];
	ofstream outputFile;
	outputFile.open("boxes.txt");
	for (int n = 0; n<nMeshes;n++)
	{
		std::tuple<float3,float3> newBox = boundingBox(meshes[n]);
		boundingBoxes[n] = newBox;
		std::get<0>(newBox);
		string s = "Cuboid[{" + std::to_string(std::get<0>(newBox).x) + "," + std::to_string(std::get<0>(newBox).y) + "," + std::to_string(std::get<0>(newBox).z) + "},{" +
			std::to_string(std::get<1>(newBox).x) + "," + std::to_string(std::get<1>(newBox).y) + "," + std::to_string(std::get<1>(newBox).z) + "}], ";
		outputFile << s;
	}

	for (float y = 0; y < screen->height; y++)
	{
		for (float x = 0; x < screen->width; x++)
		{
		    float3 pointOnScreen =  view.p1 + ((x / screen->width) * (view.p2 - view.p1) + (y / screen->height) * ( view.p3 - view.p1)) ;
			float raylength = length(pointOnScreen-view.pos);
			float3 raydirection = (pointOnScreen-view.pos) / raylength;
			float tmin = 100000;
			int meshClosest = 10000000;
			int triangleClosest = 0;
			for (int n = 0; n < nMeshes; n++)
			{
				//first check if intersection with boundingbox of mesh:
				if (RayIntersectsAABB(view.pos, raydirection, boundingBoxes[n]))
				{
					//then check triangles in this mesh:
					for (int i = 0; i < meshes[n].vcount; i+=3)
					{
						float t=RayIntersectsTriangle(view.pos, raydirection, meshes[n].vertices[i], meshes[n].vertices[i+1], meshes[n].vertices[i+2]);
						if (t && t < tmin)
						{
							tmin = t;
							meshClosest = n;
							triangleClosest = i;
						}
					}
				}	
			}
			//we now have the closest triangle (mesh index meshmin, triangleindex trianglemin) of intersection and corresponding value of t (tmin)
			if (tmin == 100000)
			{
				screen->Plot(x, y, 0xffffff);
			}
			else
			{
				int r = 255*tmin/60;
				int g = 255*tmin/60;
				int b = 255*tmin/60;
				if (meshClosest = 1)
				{
					r = 0; b = 0;
				}

				uint hexColor=((r & 0xff) << 16) + ((g & 0xff) << 8) + (b & 0xff);

				// Deze heeft voor elke ray dezelfde waarde, maar het is een andere waarde elke keer dat je het programma runt
				uint color = meshes[meshClosest].triangles[triangleClosest].material;

				screen->Plot(x, y, hexColor);
			}

	

		}

	}
	// copy pixel buffer to OpenGL render target texture
	glBindTexture( GL_TEXTURE_2D, targetTextureID );
	glTexImage2D( GL_TEXTURE_2D, 0, GL_RGBA, screen->width, screen->height, 0, GL_RGBA, GL_UNSIGNED_BYTE, screen->pixels );
}

//TODO credit here Moller-Trumbore

	

//credits to https://www.scratchapixel.com/lessons/3d-basic-rendering/minimal-ray-tracer-rendering-simple-shapes/ray-box-intersection

	
//  +-----------------------------------------------------------------------------+
//  |  RenderCore::Shutdown                                                       |
//  |  Free all resources.                                                  LH2'19|
//  +-----------------------------------------------------------------------------+
void RenderCore::Shutdown()
{
	delete screen;
}

std::tuple<float3, float3> lh2core::RenderCore::boundingBox(Mesh mesh)
{
	float minx = 10000000;
	float maxx = 0;
	float miny = 100000;
	float maxy = 0;
	float minz = 100000;
	float maxz = 0;
	for (int i = 0; i < mesh.vcount; i++)
	{
		if (mesh.vertices[i].x < minx)
		{
			minx = mesh.vertices[i].x;
		}
		if (mesh.vertices[i].x > maxx)
		{
			maxx = mesh.vertices[i].x;
		}
		if (mesh.vertices[i].y < miny)
		{
			miny = mesh.vertices[i].y;
		}
		if (mesh.vertices[i].y > maxy)
		{
			maxy = mesh.vertices[i].y;
		}
		if (mesh.vertices[i].z < minz)
		{
			minz = mesh.vertices[i].z;
		}
		if (mesh.vertices[i].z > maxz)
		{
			maxz = mesh.vertices[i].z;
		}
	}
	return std::make_tuple(make_float3(minx, miny, minz), make_float3(maxx, maxy, maxz));
}

bool lh2core::RenderCore::RayIntersectsAABB(float3 rayOrigin, float3 rayDirection, std::tuple<float3, float3> box)
{
	float3 min = std::get<0>(box);
	float3 max = std::get<1>(box);
	float tmin = (min.x - rayOrigin.x) / rayDirection.x;
	float tmax = (max.x - rayOrigin.x) / rayDirection.x;

	if (tmin > tmax) swap(tmin, tmax);

	float tymin = (min.y - rayOrigin.y) / rayDirection.y;
	float tymax = (max.y - rayOrigin.y) / rayDirection.y;

	if (tymin > tymax) swap(tymin, tymax);

	if ((tmin > tymax) || (tymin > tmax))
		return false;

	if (tymin > tmin)
		tmin = tymin;

	if (tymax < tmax)
		tmax = tymax;

	float tzmin = (min.z - rayOrigin.z) / rayDirection.z;
	float tzmax = (max.z - rayOrigin.z) / rayDirection.z;

	if (tzmin > tzmax) swap(tzmin, tzmax);

	if ((tmin > tzmax) || (tzmin > tmax))
		return false;

	if (tzmin > tmin)
		tmin = tzmin;

	if (tzmax < tmax)
		tmax = tzmax;

	return true;
}

float lh2core::RenderCore::RayIntersectsTriangle(float3 rayOrigin, float3 rayVector, float4 point0, float4 point1, float4 point2)
{
	float3 vertex0 = make_float3(point0.x, point0.y, point0.z);
	float3 vertex1 = make_float3(point1.x, point1.y, point1.z);
	float3 vertex2 = make_float3(point2.x, point2.y, point2.z);
	float3 edge1, edge2, h, s, q;
	float a, f, u, v;
	edge1 = vertex1 - vertex0;
	edge2 = vertex2 - vertex0;
	h = cross(rayVector, edge2);
	a = dot(edge1, h);
	if (a > -EPSILON && a < EPSILON)
		return false;    // This ray is parallel to this triangle.
	f = 1.0 / a;
	s = rayOrigin - vertex0;
	u = f * dot(s, h);
	if (u < 0.0 || u > 1.0)
		return false;
	q = cross(s, edge1);
	v = f * dot(rayVector, q);
	if (v < 0.0 || u + v > 1.0)
		return false;
	// At this stage we can compute t to find out where the intersection point is on the line.
	float t = f * dot(edge2, q);
	if (t > EPSILON&& t < 1 / EPSILON) // ray intersection
	{
		return t;
	}
	else // This means that there is a line intersection but not a ray intersection.
		return false;
}

// EOF